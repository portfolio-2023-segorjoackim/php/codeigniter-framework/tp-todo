<?php

namespace App\Database\Seeds;
use Myth\Auth\Password;
use Myth\Auth\src\Models\UserModel;

use CodeIgniter\Database\Seeder;

class Admin extends Seeder
{

    public function run()
    {
        $this->db->table('users')->insert($this->createAdmin());
        $this->db->table('users')->where('id',1)->update(['password_hash'=>Password::hash("Admin123")]);
        $groups = ['admin','user'];
        foreach($groups as $g){
            $this->db->table('auth_groups')->insert($this->createGroup($g));
        }
        $this->db->table('auth_groups_users')->insert(['group_id'=>1,'user_id'=>1]);
    }
    private function createAdmin():array{
        return [
            'username' => 'admin',
            'email' => 'admin@sio.com',
            'active'    => 1,
        ];
    }

    private function createGroup($name):array{
        return[
            'name'  => $name,
            'description'   => $name,
        ];
    }
}
