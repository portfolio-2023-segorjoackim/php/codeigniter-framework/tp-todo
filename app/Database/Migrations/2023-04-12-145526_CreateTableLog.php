<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTableLog extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'        => [
                'type'      =>'BIGINT',
                'constraint'        => 11,
                'unsigned'      => true,
                'auto_increment'        => true,
            ],
            'action'      =>[
                'type'      => 'VARCHAR',
                'constraint'    => '100',
                'null'      => false,
            ],
            'address'      => [
                'type'      =>  'VARCHAR',
                'constraint'    => '100',
                'null'  => false,
            ],
            'user_id'     => [
                'type'      => 'BIGINT',
                'constraint'    =>11,
                'null'  => false,
            ],
            
            'task_id'      => [
                'type'  =>'BIGINT',
                'constraint' =>11,
                'null'  => true,
            ],
            'created_at'     => [
                'type'      => 'DATETIME',
                'null'  => false,
            ],
        ]);
        $this->forge->addKey('id',true);
        $this->forge->createTable('log');
    }

    public function down()
    {
        $this->forge->dropTable('log');
    }
}
