<?= $this->extend('page.php')?>
<?= $this->section('body')?>
<br>
<div class="card">
    <div class="card-header">
        <h1>Profil Utilisateur</h1>
    </div>
    <div class="card-body">
        <form class="form-horizontal" action ="/save-profil" method="post" >
        <div class=form-group>
            <label for="username">Username</label>
            <input class="form-control"  name="username" class ="username" type ="text" value = <?= user()->username?> />
            <br>
            <label for="email">Email</label>
            <input class="form-control"  name="email" class="email" type ="text" value = <?= user()->email?> />
            <br>
            <label for="password">Password</label>
            <input class="form-control"  name="password" type ="password" readonly value =<?= user()->password_hash?>/>
        </div>
            <button class="btn btn-primary" type="submit" >Valider</button>
        </form>
    </div>
</div>
<?php $this->endSection('body')?>