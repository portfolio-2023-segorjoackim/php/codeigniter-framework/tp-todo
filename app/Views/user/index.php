<?= 
$this->extend('page.php');
$this->section('body');
?>
<div class="card">
    <div class="card-header">
        <h1>Welcome to your tasks manager</h1>
    </div>
    <div class="card-body">
        <p>Chers employés,<br>
            Je vous souhaite la bienvenue sur le site ToDo qui va nous permettre,
            de mieux organiser les tâches de l'entreprise.
            Que vous soyez secrétaire,développeur(euse), toutes les tâches que vous effecturez
            seront sur ce site c'est pourquoi je compte sur vous pour l'avancé de l'entreprise,
            et la hausse de vos salaires.<br>

            Cordialement,<br>

            Le Chef Joackim SÉGOR.
        </p>
    </div>
</div>    

<?= $this->endSection() ?>