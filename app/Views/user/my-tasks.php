<?php
    
    $this->extend('page.php');
    $this->section('body');
?>
<div class="card">
    <form method="post">
    <div class="card-header">
        <h1>Mes tâches</h1>
    </div>
    <div class="card-body">
    <?= view('/messages/message_task');?>
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col-1">#</th>
                    <th scope="col-6">Tâche</th>
                    <th scope="col-3">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($tasks as $task): ?>
                    <tr>
                        <th class="col-1"><?= $task->order ?></th>
                        <?php if($task->done) : ?>
                            <td class="col-6"><s><?= $task->text ?></s></td>
                            <td class="col-3">
                            <a href="<?='/edit-mine/'.$task->id ?>" class="btn btn-primary row-1" >
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="<?='/del-mine/'.$task->id ?>" class="btn btn-danger row-2">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                            <a href="<?= '/done/'.$task->id ?>" class="btn btn-secondary row-3" role="button">
                                <i class="fa fa-check-square"></i>
                            </a>
                        </td>
                        <?php else : ?>
                            <td class="col-6"><?= $task->text ?></td>
                            <td class="col-3">
                            <a href="<?='/edit-mine/'.$task->id ?>" class="btn btn-primary row-1" >
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="<?='/del-mine/'.$task->id ?>" class="btn btn-danger row-1">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                            <a href="<?= '/done/'.$task->id ?>" class="btn btn-success row-1" role="button">
                                <i class="fa fa-check-square"></i>
                            </a>
                        </td>
                        <?php endif ?>
                    
                        
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
    </form>
</div>
<?= $this->endSection() ?>
        
