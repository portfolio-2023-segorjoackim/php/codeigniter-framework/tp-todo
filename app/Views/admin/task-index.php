<?php
    
    $this->extend('page.php');
    $this->section('body');
?>
<div class="card">
    <form method="post">
    <div class="card-header">
        <h1>Les tâches</h1>
    </div>
    <div class="card-body">
    <?= view('/messages/message_task');?>
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col-1">#</th>
                    <th scope="col-5">Tâche</th>
                    <th scope="col-3">Action</th>
                    <th scope="col">User</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($tasks as $task): ?>
                    <tr>
                        <th class="col-1"><?= $task->order ?></th>
                        <?php if($task->done) : ?>
                            <td class="col-5"><s><?= $task->text ?></s></td>
                            <td class="col-3 row-1">
                                <a href="<?= '/done/'.$task->id ?>" class="btn btn-secondary " role="button">
                                    <i class="fa fa-check-square"></i>
                                </a>
                                <a href="<?='/modifier/'.$task->id ?>" class="btn btn-primary " >
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="<?='/supprimer/'.$task->id ?>" class="btn btn-danger ">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </td>
                        <?php else : ?>
                            <td class="col-5"><?= $task->text ?></td>
                            <td class="col-3 row-1">
                                <a href="<?= '/done/'.$task->id ?>" class="btn btn-success " role="button">
                                    <i class="fa fa-check-square"></i>
                                </a>
                                <a href="<?='/modifier/'.$task->id ?>" class="btn btn-primary " >
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="<?='/supprimer/'.$task->id ?>" class="btn btn-danger ">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </td>
                        <?php endif ?>
                        <?php foreach($profils as $p): ?>
                            <?php if($p->id == $task->user_id):?>
                                <td class="col-3 md-1"><?= $p->username ?></td>
                            <?php endif?>
                        <?php endforeach ?>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
    <div class=card-footer>
        <div class=col-12>
            <a class="btn btn-primary col-12" href="/creer"><i class="fas fa-plus" aria-hidden="true">Ajouter une tâche</i></a>
            <a href="/reorder" class="btn btn-secondary active col-12 mt-1" role="button">
                <i class="fa fa-sort-numeric-asc" aria-hidden="true">Réordonner</i>
            </a>
        </div>
        
    </div>
    </form>
</div>
<?= $this->endSection() ?>
        
