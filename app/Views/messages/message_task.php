<?php if (session()->has('t-message')) : ?>
	<div class="alert alert-success">
		<?= session('t-message') ?>
	</div>
<?php endif ?>

<?php if (session()->has('t-error')) : ?>
	<div class="alert alert-danger">
		<?= session('t-error') ?>
	</div>
<?php endif ?>

<?php if (session()->has('t-errors')) : ?>
	<ul class="alert alert-danger">
	<?php foreach (session('t-errors') as $error) : ?>
		<li><?= $error ?></li>
	<?php endforeach ?>
	</ul>
<?php endif ?>