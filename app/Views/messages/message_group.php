<?php if (session()->has('g-message')) : ?>
	<div class="alert alert-success">
		<?= session('g-message') ?>
	</div>
<?php endif ?>

<?php if (session()->has('g-error')) : ?>
	<div class="alert alert-danger">
		<?= session('g-error') ?>
	</div>
<?php endif ?>

<?php if (session()->has('g-errors')) : ?>
	<ul class="alert alert-danger">
	<?php foreach (session('g-errors') as $error) : ?>
		<li><?= $error ?></li>
	<?php endforeach ?>
	</ul>
<?php endif ?>