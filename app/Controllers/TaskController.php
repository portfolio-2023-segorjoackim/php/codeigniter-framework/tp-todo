<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Entities\Task;
use App\Models\TaskModel;
use App\Models\LogModel;
use App\Models\ProfilModel;

class TaskController extends BaseController
{
    public bool $admin;

    public function __construct()
    {
        //ON place dans le constructeur toutes les classes nécessaires lors de l'appel 
        //des différentes méthodes Attention au double underscore de construct()
        //les helpers sont des bibliothèques de classes et de fonctions 
        // que l'on va utiliser lors du développement. On les charge dans le constructeur
        $this->taskModel=new TaskModel();
        $this->profilModel=new ProfilModel();
        $this->logModel = new LogModel();
        
        foreach(user()->getRoles() as $role){
            if($role == 'admin'){
                $this->admin = true;
            }else{
                $this->admin = false;
            }
        }
    }

    public function index()
    {
        
        $tasks = $this->taskModel->orderBy('order')->findAll();
        $profils = $this->profilModel->findAll();
        //On rassemble toutes les données utilisées par la vue dans un tableau $data
        $data = [
            'tasks' => $tasks,
            'titre' =>  "Au boulot",
            'user' => user(),
            'profils' =>$profils,
        ];
        //chacun des éléments du tableau $data sera accessible dans la vue 
        //view est une méthode héritée de BaseController
        if($this->admin == true){
            return view('/admin/task-index.php',$data);
        }
        return view('/user/task-index.php',$data);
        
    }

    //Il va falloir prévoir un nouveau formulaire permettant de créer une nouvelle tâche
    public function create()
    {
        $data =[
            'titre' => 'Nouvelle Tâche',
            'user' => user(),
        ];
        $this->logModel->logger('wadd');
        return view('/admin/task-form.php',$data);
    }

    // La méthode prévoit de recevoir l'id de la tâche à supprimer
    public function delete(int $id)
    {
        $this->taskModel->where(['id'=> $id])->delete();
        $this->logModel->logger('del',$id);
        $this->autoReorder();
        return redirect()->to('/task-index')->with('t-error','Tâche supprimée');
    }

    // La méthode prévoit de recevoir l'id mais par défaut elle ne recevra aucun paramètre
    // c'est le seul moyen de creéer une surcharge en php
    public function save(int $id = null)
    {
        //Définir les règles de validation du formulaire 
        // Que l'on récupère de TaskMmodel
        $rules = $this->taskModel->getValidationRules();
        //On vérifie que l'on passe la validation
        if(!$this->validate($rules))
        {
            // en cas d'erreur on redirige vers la page précédente
            return redirect()->back()->withInput()->with('t-errors',$this->validator->getErrors());
        }
        else
        {
            // en cas de succès de la validation
            // On récupère les donnée du formulaire
            $form_data = [
                'text'=> $this->request->getPost('text'),
                'order'=> $this->request->getPost('order'),
                'user_id'=>user()->id,
            ];
            // Si l'id n'est pas null on l'ajoute dans les données à transmettre
            if(!is_null($id))
            {
                $form_data['id']=$id;
                $this->logModel->logger('edit',$id);
            }else{
                $this->logModel->logger('add');
            }
            // Créer une instance de 1notre tache
            $task = new Task($form_data);
            // Si l'id est renseigné save effectue un update sinon un insert
            $this->taskModel->save($task);
            $this->autoReorder();
            return redirect()->to('/task-index')->with('t-message',"Tâche sauvegardée");
        }
    }

    public function edit(int $id)
    {
        // on récupère la tâche à modifier
        $data=[
            'titre' => "Modifier tâche",
            'task' => $this->taskModel->find($id),
            'user' => user(),
        ];
        $this->logModel->logger('wedit',$id);
        // on appelle la vue
        return view('admin/task-form',$data);
    }
    public function done(int $id)
    {
        $task = $this->taskModel->find($id);
        
        if($task->done == 1){
            $this->taskModel->update($id,['done'=>'0']);
            $this->logModel->logger('nmt',$id);
            return redirect()->back()->with('t-error','Tâche non faite');
        }else{
            $this->taskModel->update($id,['done'=>'1']);
            $this->logModel->logger('mt',$id);
            return redirect()->back()->with('t-message','Tâche faite');
        }
        
    }

    public function indexReorder()
    {
        $tasks = $this->taskModel->orderBy('order')->findAll();
        $index = 1;
        //on numérote l'ordre de toutes les tâches.
        foreach($tasks as $task)
        {
            $task->order = $index;
            $index += 1;
        }
        $data=[
            'titre' => "Réordonner les tâches",
            'tasks' => $tasks,
            'user' => user(),
        ];
        return view('admin/task-indexReorder.php',$data);

    }

    public function saveReorder()
    {
        $validation = \Config\Services::validation();
        $validation->setRule('order.*','ordre','required|numeric');
        if(!$validation->withRequest($this->request)->run())
        {
            return redirect()->back()->withInput()->with('t-errors',$validation->getErrors());
        }else{
            $orders = $this->request->getPost('order[]');
            $ids = $this->request->getPost('id[]');
            $index = 0;
            foreach($ids as $id)
            {
                $form_data = [
                    'order' => $orders[$index],
                    'id'    => $ids[$index],
                ];
                $task = new Task($form_data);
                $this->taskModel->save($task);
                $index++;
            }
            return redirect()->to('/task-index')->with('t-message',"Tâches réorganisées");
        }
    }

    public function mine(){
        $tasks = $this->taskModel->where('user_id',user()->id)->orderBy('order')->findAll();
        //On rassemble toutes les données utilisées par la vue dans un tableau $data
        $data = [
            'tasks' => $tasks,
            'titre' =>  "Mes tâches",
            'user' => user(),
        ];
        return view('/user/my-tasks.php',$data);
    }

    private function autoReorder()
    {
        $i = 1;
        $tasks = $this->taskModel->orderBy('order')->findAll();
        foreach($tasks as $task){
            $param = [
                'id' => $task->id,
                'order' => $i,
            ];
            $task = new Task($param);
            $this->taskModel->save($task);
            $i++;
        }
    }


    
}
