<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ProfilModel;
use App\Entities\Profil;

class ProfilController extends BaseController
{
    private $profilModel;
    public function __construct()
    {
        //ON place dans le constructeur toutes les classes nécessaires lors de l'appel 
        //des différentes méthodes Attention au double underscore de construct()
        //les helpers sont des bibliothèques de classes et de fonctions 
        // que l'on va utiliser lors du développement. On les charge dans le constructeur
        $this->profilModel=new ProfilModel();
    }
    public function index()
    {
        $data = [
            'user'  => user(),
        ];
        return view('user/profil',$data);
    }

    public function save(){
        $form_data = [
            'username'=> $this->request->getPost('username'),
            'email'=> $this->request->getPost('email'),
        ];
        //var_dump($form_data);
        $profilModif = $this->profilModel->find(user()->id);   
        $profilModif->username = $this->request->getPost('username');
        $profilModif->email = $this->request->getPost('email');
        var_dump($profilModif->username);
        var_dump($profilModif->email);
        $this->profilModel->where('id', user()->id)->save($profilModif);
        return redirect()->to('/');

        
    }
}
