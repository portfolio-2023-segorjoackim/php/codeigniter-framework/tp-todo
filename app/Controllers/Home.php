<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        if(isset(user()->id)){
            return view('user/index',['user'=>user()]);
        }
        return view('welcome_todo');
    }
    public function attempt(){
        return view('user/index',['user'=>user()]);
    }
}
