<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Entities\Log;

class LogModel extends Model
{
    //protected $DBGroup          = 'default';
    protected $table            = 'log';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['action','address','user_id','task_id','created_at'];

    // Dates
    protected $useTimestamps = true;
    
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    
    protected $updatedField  = null;
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [
    
    ];
/*
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
    */
    private $comp;

    public function logger($option,$task_id =null){
        switch($option){
            case 'wadd':
                $this->comp = [
                    'action' => 'Veut créer une tâche',
                    'address' =>''.$_SERVER['REMOTE_ADDR'],
                    'user_id'  => user()->id,
                ];
                break;
             case 'add':
                $this->comp = [
                    'action' => 'A créer une tâche',
                    'address' =>''.$_SERVER['REMOTE_ADDR'],
                    'user_id'  => user()->id,
                ];
                break;
             case 'edit':
                $this->comp = [
                    'action' => 'A modifier une tâche',
                    'address' =>''.$_SERVER['REMOTE_ADDR'],
                    'user_id'  => user()->id,
                    'task_id'      => $task_id,
                ];
                break;
            case 'wedit':
                if(!is_null($task_id)){
                    $this->comp = ['action' => 'Veut modifier une tâche',
                    'address' =>''.$_SERVER['REMOTE_ADDR'],
                    'user_id'  => user()->id,
                    'task_id'      => $task_id,
                    ];
                }
                break;
            case 'del':
                if(!is_null($task_id)){
                    $this->comp = ['action' => 'A supprimer une tache',
                    'address' =>''.$_SERVER['REMOTE_ADDR'],
                    'user_id'  => user()->id,
                    'task_id'      => $task_id,
                    ];
                }
                break;
            case 'mt':
                if(!is_null($task_id)){
                    $this->comp = ['action' => 'A fait une tâche',
                    'address' =>''.$_SERVER['REMOTE_ADDR'],
                    'user_id'  => user()->id,
                    'task_id'      => $task_id,
                    ];
                }
                break;
            case 'nmt':
                if(!is_null($task_id)){
                    $this->comp = ['action' => 'N\'a pas fait une tâche',
                    'address' =>''.$_SERVER['REMOTE_ADDR'],
                    'user_id'  => user()->id,
                    'task_id'      => $task_id,
                    ];
                }
                break;
                
        }
        $this->save(new Log($this->comp));
    }
}
