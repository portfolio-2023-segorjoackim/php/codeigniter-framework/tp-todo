# CodeIgniter 4 Projet ToDo

## Cas d'utilisation

```plantuml
@startuml
left to right direction

actor Guest as g
actor Admin as a
actor Admin as al
actor User as u
actor User as ul

package ToDo{
  usecase login as l
  usecase register as r
}
a --> l
u --> l
g --> l
g --> r

l --|> ul
l --|> al
r --|> ul

package "ToDo Admin"{
  usecase CreateTask as ct
  usecase EditTask as et
  usecase DoneTask as dt
  usecase DeleteTask as Dt
  usecase ReorderTask as rt
}
package "ToDo Admin and User"{
  usecase EditProfil as ep
  usecase Logout as lt
}
package "ToDo User"{
  usecase CreateHisTask as cht
  usecase EditHisTask as eht
  usecase DoneHisTask as dht
  usecase DeleteHisTask as Dht
  
}

al --> ct
al -->et
al -->dt
al --> Dt
al -->rt
al --> ep
al --> lt

ul --> cht
ul --> eht
ul --> dht
ul --> Dht
ul --> ep
ul --> lt







@enduml
```
---
## Table Log and Task

```plantuml
@startuml

Log : int:id
Log : string:action
Log : string:address
Log : int:user_id
Log : int:task_id
Log : datetime:created_at

Log : logger(string:option,int:task_id)

Task : int:id
Task : string:text
Task : bool:done
Task : int:order
Task : int:user_id


@enduml
```

---

## Guest views

### Home view

![guest-home-view](assets/guest-home.jpg)

### Login view

When a Guest user (guest or non-authenticated) presses the TextLink "Tasks" he is sent to the login page.

![guest-login-view](assets/guest-login.jpg)

### Register view

![guest-register-view](assets/guest-register.jpg)

---

## Same view for all authentified user


### Authentified User Home view 

![login-home-view](assets/login-home.jpg)

### Authentified User Profil view

On this page you can edit your username and your email only

![login-profil-view](assets/login-profil.jpg)

---

## User views


### Create task

![user-create-task](assets/user-create-task.jpg)

### Tasks view 

As you can see an user can make action only on his tasks  

![user-tasks-view](assets/user-task.jpg)

### After make view

![user-make-task-view](assets/user-make-task.jpg)

### Edit his task view

 We going to edit task 2 "Hihihi" and replace it with "Test edit"
 To realize this you click on the blue button with the pen icon 

![user-edit-task-view](assets/user-edit-task.jpg)

You can see the result with your own eyes

![user-task-after-edit-view](assets/user-task-afe.jpg)

### Delete his Task 

To delete task just click on the red button with bin icon
For example we going to delete the task "Test edit"

![user-delete-task](assets/user-delete-task.jpg)

You can see the tasks are automatically reorded

### My Tasks view

User has a view to see his tasks and edit,done and delete these more easily
![user-mine-task](assets/user-mine-task.jpg)

---

## Admin views

### Create Task view

As you can see user and admin have the same page to create task

![admin-create-task-view](assets/admin-create-task.jpg)

### Tasks View

As you can see admin can make action on all tasks even those that don't belong to him
And he can see who is the owner task and reorder these

![admin-task-view](assets/admin-task.jpg)

### Edit Task view

Like to create a task user and admin have the same page
We going to edit "hoho" and replace it with "Edit admin"

![admin-edit-task-view](assets/admin-edit-task.jpg)

You can see the result

![admin-edit-task-view](assets/admin-task-afe.jpg)

### Reorder view

You can see I have exchange the values order between "Edit admin" and "Tâche admin"

![admin-reorder-view](assets/admin-reorder.jpg)

You can see the result

![admin-reorder-view](assets/admin-task-afr.jpg)


---

## Log Example

![log-table](assets/log-example.jpg)

## What is CodeIgniter?

CodeIgniter is a PHP full-stack web framework that is light, fast, flexible and secure.
More information can be found at the [official site](https://codeigniter.com).

This repository holds a composer-installable app starter.
It has been built from the
[development repository](https://github.com/codeigniter4/CodeIgniter4).

More information about the plans for version 4 can be found in [CodeIgniter 4](https://forum.codeigniter.com/forumdisplay.php?fid=28) on the forums.

The user guide corresponding to the latest version of the framework can be found
[here](https://codeigniter4.github.io/userguide/).

## Installation & updates

`composer create-project codeigniter4/appstarter Todo` then `composer update` whenever
there is a new release of the framework.

When updating, check the release notes to see if there are any changes you might need to apply
to your `app` folder. The affected files can be copied or merged from
`vendor/codeigniter4/framework/app`.

You need to make 2 dependances:
`` composer require codeigniter4/translations ``
`` composer require myth/auth ``

Create your database and modifying of file `.env`
CI_ENVIRONMENT value = development.
For other parameters insert your data to connect your database to your site

![modifying-env](assets/env.jpg)

After doing all this make a ``php spark migrate -all`` on your terminal to create all table into your database

And make a ``php spark db:seed Admin`` to make users groups and admin user

By default config the admin authentification is

 username : ``admin`` 
 password ``Admin123``

For another user you can create it on the website on the page register

## Setup

Copy `env` to `.env` and tailor for your app, specifically the baseURL
and any database settings.

## Important Change with index.php

`index.php` is no longer in the root of the project! It has been moved inside the *public* folder,
for better security and separation of components.

This means that you should configure your web server to "point" to your project's *public* folder, and
not to the project root. A better practice would be to configure a virtual host to point there. A poor practice would be to point your web server to the project root and expect to enter *public/...*, as the rest of your logic and the
framework are exposed.

**Please** read the user guide for a better explanation of how CI4 works!

## Repository Management

We use GitHub issues, in our main repository, to track **BUGS** and to track approved **DEVELOPMENT** work packages.
We use our [forum](http://forum.codeigniter.com) to provide SUPPORT and to discuss
FEATURE REQUESTS.

This repository is a "distribution" one, built by our release preparation script.
Problems with it can be raised on our forum, or as issues in the main repository.

## Server Requirements

PHP version 7.4 or higher is required, with the following extensions installed:

- [intl](http://php.net/manual/en/intl.requirements.php)
- [mbstring](http://php.net/manual/en/mbstring.installation.php)

Additionally, make sure that the following extensions are enabled in your PHP:

- json (enabled by default - don't turn it off)
- [mysqlnd](http://php.net/manual/en/mysqlnd.install.php) if you plan to use MySQL
- [libcurl](http://php.net/manual/en/curl.requirements.php) if you plan to use the HTTP\CURLRequest library
